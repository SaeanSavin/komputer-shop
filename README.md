# Background
Komputer Store is a mandatory assignment at Noroff Accelerate. This assignment only uses vanilla javascript and only need a web browser and a internet connection for product information from the API. 


# Komputer Store

Welcome to Komputer Store! 
When browsing you can look through a handful of laptops and each one has it own specification, description and price.
You can earn money by either work or take a loan, but you can only take one loan at a time. The loan can not exceed two times the amount of current bank balance. 
You need to pay 10% of the current salary to the loan when transfering money to the bank. Or you can use Pay Loan button to pay all at once. 
It has a dropdown for all product from the API and you can buy them (if you have enough money in the bank)


## Requirements
```
 -  A web browser
 - Internet connection (For API calls)
```


## Install

#### Clone repository:
```
   git clone https://gitlab.com/SaeanSavin/komputer-shop.git
```

## Usage

```
 - Open with a text editor Example: Visual Studio Code
 - Install LiveServer plugin for Visual Studio Code
 - Rightclick index.html file and select "Open with live server"

 - Also works by opening the index.html file.
```
