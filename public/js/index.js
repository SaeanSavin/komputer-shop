const currentBalances = {
    BankBalance: 0,
    SalaryBalance: 0,
    Loan: 0,
};

const products = [];
let currentProductIndex = 0;

const fallbackImageSource = "./images/no-image.jpg";

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then((response) => {
        return response.json();
    })
    .then((productResponse) => {
        products.push(...productResponse);
        renderProductSelection();
        renderProductInformation(0);
    });

const balanceElement = document.getElementById("current-balance");
const salaryBalanceElement = document.getElementById("current-salary");
const loanBalanceElement = document.getElementById("current-loan");
const loanElement = document.getElementById("loan-button");
const workElement = document.getElementById("work-button");
const bankElement = document.getElementById("bank-button");
const payLoanElement = document.getElementById("pay-button");
const productSelectorElement = document.getElementById("product-select");
const productImageElement = document.getElementById("product-image");
const productSpecsElement = document.getElementById("product-info-specs");
const productDescriptionElement = document.getElementById("product-description");
const productPriceElement = document.getElementById("product-price");
const buyProductElement = document.getElementById("buy-product-button");

loanElement.addEventListener("click", function () {
    getLoan(currentBalances);
});
workElement.addEventListener("click", function () {
    work(currentBalances);
});
bankElement.addEventListener("click", function () {
    transferToBank(currentBalances);
});
payLoanElement.addEventListener("click", function () {
    payLoan(currentBalances, 100);
});
productSelectorElement.addEventListener("change", function (event) {
    renderProductInformation(event.target.value);
});
buyProductElement.addEventListener("click", function () {
    buyProduct(currentBalances);
});

function updateBalances(balances) {
    balanceElement.innerHTML = "Balance: " + balances.BankBalance + " kr.";
    salaryBalanceElement.innerHTML =
        "Salary: " + balances.SalaryBalance + " kr.";
    loanBalanceElement.innerHTML = "Loan: " + balances.Loan + " kr.";
    loanBalanceElement.style.display = hasLoan(balances.Loan)
        ? "block"
        : "none";
    payLoanElement.style.display = hasLoan(balances.Loan) ? "block" : "none";
}

function getLoan(balanaces) {
    if (hasLoan(balanaces.Loan)) {
        window.alert("Need to pay current loan before take a new one");
        return;
    }

    let newLoan = parseFloat(prompt("How much do you want to loan?"));

    if (isNaN(newLoan)  || newLoan < 0) {
        window.alert("Invalid input! Please enter a number");
        return;
    }

    if (balanaces.Loan > 0) {
        window.alert(`Can't loan more money from bank please pay ${balanaces.Loan} back before take a new loan.`);
        return;
    }

    if(newLoan > balanaces.BankBalance * 2) {
        window.alert(`You can't loan more than 2x the balance. Current: ${balanaces.Loan}.`);
        return;
    }

    balanaces.BankBalance += newLoan;
    balanaces.Loan = newLoan;
    updateBalances(balanaces);
}

function payLoan(balances, percentage) {

    if (Number(balances.Loan) === 0 || Number(balances.SalaryBalance) === 0) {
        window.alert("You don't have any loan or don't have any money to pay the loan.");
        return;
    }

    if(percentage === 100 && balances.SalaryBalance >= balances.Loan) {
        balances.SalaryBalance -= balances.Loan;
        balances.Loan = 0;
    }
    else if(percentage === 100 && balances.SalaryBalance <= balances.Loan) {
        balances.Loan -= balances.salaryBalance;
        balances.SalaryBalance = 0;
    }
    else {
        let loanToPay = getPercentageOfAmount(balances.SalaryBalance, percentage);

        if (balances.SalaryBalance <= loanToPay) {
            balances.Loan -= loanToPay;
            balances.SalaryBalance -= loanToPay;
        }
        else if (balances.SalaryBalance >= loanToPay) {
            if(loanToPay > balances.Loan) {
                loanToPay -= balances.Loan;  
                balances.Loan = 0;    
            }
            else {
                balances.Loan -= loanToPay;
            }
            balances.SalaryBalance -= loanToPay;
        }
    }

    updateBalances(balances);
}

function hasLoan(amount) {
    return amount > 0;
}

function work(balances) {
    balances.SalaryBalance += 100;
    updateBalances(balances);
}

function getPercentageOfAmount(salary, amount) {
    return (salary * amount) / 100
}

function buyProduct(balances) {
    if (balances.BankBalance >= products[currentProductIndex].price) {
        window.alert(`You have purchased ${products[currentProductIndex].title}. Congratulations!`);
        balances.BankBalance -= products[currentProductIndex].price;
    } else {
        window.alert("You don't have enough money for this product.");
    }

    updateBalances(balances);
}

function transferToBank(balances) {
    if (balances.SalaryBalance === 0) {
        window.alert("You don't have any money to transfer");
        return;
    }

    if (hasLoan(balances.Loan)) {
        payLoan(balances, 10);
    }
    balances.BankBalance += balances.SalaryBalance;
    balances.SalaryBalance = 0;
    updateBalances(balances);
}

function renderProductSelection() {
    for (let i = 0; i < products.length; i++) {
        const html = `
            <option value="${i}">${products[i].title}</option>
        `;
        productSelectorElement.insertAdjacentHTML("beforeend", html);
    }
}

function renderProductInformation(index) {
    
    productImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + products[index].image;

    productSpecsElement.innerHTML = "";
    productDescriptionElement.innerHTML = "";

    currentProductIndex = index;

    for (let i = 0; i < products[index].specs.length; i++) {
        const html = `
            <li>${products[index].specs[i]}</li>
        `;
        productSpecsElement.insertAdjacentHTML("beforeend", html);
    }

    const html = `<p>${products[index].description}</p>`;
    productDescriptionElement.insertAdjacentHTML("beforeend", html);

    productPriceElement.innerHTML = products[index].price + "kr.";
}

productImageElement.addEventListener("error", function () {
    productImageElement.src = fallbackImageSource;
});

updateBalances(currentBalances);